﻿using System;
namespace NAM_Login.Request
{
    public class TokenIntrospectRequest
    {

		public TokenIntrospectRequest(string token)
		{
			this.token = token;
		}
		public string token { get; set; }
    }
}

