﻿using System.Text.Json.Serialization;
using System;

namespace NAM_Login.Request
{
	public class TokenRequest
	{
		public TokenRequest(string client_id, string client_secret, string code, string grant_type, string redirect_uri)
		{
			this.client_id = client_id;
			this.client_secret = client_secret;
			this.code = code;
			this.grant_type = grant_type;
			this.redirect_uri = redirect_uri;
		}


		[JsonPropertyName("client_id")]
		public string client_id { get; set; }

		[JsonPropertyName("client_secret")]
		public string client_secret { get; set; }

		[JsonPropertyName("code")]
		public string code { get; set; }

		[JsonPropertyName("grant_type")]
		public string grant_type { get; set; }

		[JsonPropertyName("redirect_uri")]
		public string redirect_uri { get; set; }

	}
}

