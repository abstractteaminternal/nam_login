﻿using System.Text.Json.Serialization;
namespace NAM_Login.Request
{
		public class RefreshTokenRequest
	{
        public RefreshTokenRequest(string client_id, string client_secret, string grant_type, string refresh_token)
        {
            this.client_id = client_id;
            this.client_secret = client_secret;
            this.grant_type = grant_type;
			this.refresh_token = refresh_token;

		}

		[JsonPropertyName("client_id")]
		public string client_id { get; set; }

		[JsonPropertyName("client_secret")]
		public string client_secret { get; set; }


		[JsonPropertyName("grant_type")]
		public string grant_type { get; set; }

		[JsonPropertyName("refresh_token")]
		public string refresh_token { get; set; }
				
	}
}

