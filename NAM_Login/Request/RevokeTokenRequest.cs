﻿using System;
using System.Text.Json.Serialization;
namespace NAM_Login.Request
{
	public class RevokeTokenRequest
	{
		public RevokeTokenRequest(string client_id, string client_secret, string token)
		{
			this.client_id = client_id;
			this.client_secret = client_secret;
			this.token = token;

		}

		[JsonPropertyName("client_id")]
		public string client_id { get; set; }

		[JsonPropertyName("client_secret")]
		public string client_secret { get; set; }

		[JsonPropertyName("token")]
		public string token { get; set; }
	}
}

