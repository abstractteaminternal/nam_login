﻿using System.Text.Json.Serialization;

namespace NAM_Login.Models
{
	public class TokenModel
	{

		[JsonPropertyName("access_token")]
		public string access_token { get; set; }

		[JsonPropertyName("token_type")]
		public string token_type { get; set; }

		[JsonPropertyName("expires_in")]
		public int expires_in { get; set; }

		[JsonPropertyName("refresh_token")]
		public string refresh_token { get; set; }

		[JsonPropertyName("id_token")]
		public string id_token { get; set; }


	}
}

