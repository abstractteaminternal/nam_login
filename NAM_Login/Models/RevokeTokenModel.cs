﻿using System;
using System.Text.Json.Serialization;

namespace NAM_Login.Models
{
	public class RevokeTokenModel
	{

		[JsonPropertyName("isSuccess")]
		public bool isSuccess { get; set; }

		[JsonPropertyName("error")]
		public string error { get; set; }

		[JsonPropertyName("error_description")]
		public string error_description { get; set; }

	}
}

