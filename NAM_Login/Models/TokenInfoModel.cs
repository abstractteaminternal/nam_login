﻿using System.Text.Json;
using System;
using System.Text.Json.Serialization;

namespace NAM_Login.Models

{
	public class TokenInfoModel
	{
		[JsonPropertyName("active")]
		public bool active { get; set; }

		[JsonPropertyName("username")]
		public string username { get; set; }
	}
}
