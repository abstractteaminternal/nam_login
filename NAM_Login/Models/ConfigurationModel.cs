﻿using System.Text.Json.Serialization;

namespace NAM_Login.Models
{
	public class ConfigurationModel
	{
		
        [JsonPropertyName("token_endpoint")]
		public string token_endpoint { get; set; }

		[JsonPropertyName("revocation_endpoint")]
		public string revocation_endpoint { get; set; }

		[JsonPropertyName("introspection_endpoint")]
		public string introspection_endpoint { get; set; }

    }
}

