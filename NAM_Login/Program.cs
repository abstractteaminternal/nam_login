﻿using Microsoft.EntityFrameworkCore;
using NAM_Login.TetraServices;
using NAM_Login.Models;

namespace Login
{
    class Prelogin
    {

        static readonly ITetraService service = new TetraService();
        static ConfigurationModel? confModel;

        public static void Main(string[] args)
        {

            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            confModel = service.GetNAMConfiguration().Result.Result;

            initializeServices(builder);


        }

        private static void initializeServices(WebApplicationBuilder builder)
        {

            var app = builder.Build();

            app.MapPost("/api/prelogin", async (string code) =>
            {
                Console.WriteLine("prelogin called. code: " + code);

                var token = service.GetToken(code).Result.Result;
                Console.WriteLine("JWT token: " + token.access_token);
                Console.WriteLine("Refresh token: " + token.refresh_token);

                var infoToken = (await service.GetTokenInfo(token.access_token)).Result;
                Console.WriteLine("User logged: " + infoToken.username);

                return infoToken;

            });

            app.MapGet("/api/token", (string authCode) =>
            {
                Console.WriteLine(authCode);
                return global::System.Threading.Tasks.Task.CompletedTask;
            });


            app.MapPost("/api/refreshToken", async (string refreshToken) =>
            {
                Console.WriteLine("refreshToken called. Token: " + refreshToken);
                var result = service.RefreshToken(refreshToken).Result.Result;
                Console.WriteLine("JWT token: " + result.access_token);
                Console.WriteLine("Refresh token: " + result.refresh_token);
                Console.WriteLine("refreshToken end");
                return result;
            });

            app.MapPost("/api/revokeToken", (string refreshToken) =>
            {
                Console.WriteLine("revokeToken called. refreshToken: " + refreshToken);
                var result = service.RevokeToken(refreshToken);
                Console.WriteLine("revokeToken end");
                return result;
            });


            app.MapPost("/api/introspectToken", (string token) =>
            {
                Console.WriteLine("introspectToken called. Token: " + token);
                var infoToken = service.GetTokenInfo(token).Result;
                Console.WriteLine("introspectToken end");
                return infoToken;
            });


            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();

        }
    }
}