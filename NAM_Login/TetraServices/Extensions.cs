﻿using System;
using System.Text;
using Newtonsoft.Json;

namespace TetraServices.Common
{
    public static class Extensions
    {
        public static string ToCamelCase(this string val)
        {
            char prevChar = val[0];

            var sb = new StringBuilder();

            for (int i = 0; i < val.Length; i++)
            {
                if (i > 0)
                {
                    if (val[i] == ' ')
                        continue;

                    if (prevChar != ',')
                    {
                        sb.Append(val[i]);
                    }
                    else
                    {
                        sb.Append(char.ToLowerInvariant(val[i]));
                    }
                    prevChar = val[i];
                }
                else
                {
                    sb.Append(char.ToLowerInvariant(val[0]));
                }
            }

            return sb.ToString();

        }

    }

    public static class Utility
    {
        public static Dictionary<string, TValue> ToDictionary<TValue>(object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, TValue>>(json);
            return dictionary;
        }
    }
    
}
