﻿
namespace NAM_Login.TetraServices
{
    public class TetraServiceException : Exception
    {

        string message = string.Empty;
        string code = string.Empty;

        public TetraServiceException(ErrorResponse responseMsg)
        {
            this.message = responseMsg.Error.Message;
            this.code = responseMsg.Error.Code;
        }


        /// <summary>
        /// Response Message
        /// </summary>
        public override string Message => this.message;

        /// <summary>
        /// Response Code
        /// </summary>
        public string Code => this.code;
    }
}
