﻿using System;
using NAM_Login.Models;

namespace NAM_Login.TetraServices
{ 

	public interface ITetraService
	{
		Task<Response<ConfigurationModel>> GetNAMConfiguration();
		Task<Response<TokenModel>> GetToken(string code);
		Task<Response<TokenModel>> RefreshToken(string refreshToken);
		Task<Response<TokenInfoModel>> GetTokenInfo(String token);
		Task<Response<RevokeTokenModel>> RevokeToken(String refreshToken);
	}

}
