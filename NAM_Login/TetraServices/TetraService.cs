﻿using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using NAM_Login.Models;
using NAM_Login.Request;
using TetraServices.Common;

namespace NAM_Login.TetraServices
{

    public class TetraService: ITetraService
    {
        private HttpClient client;
        private  IConfiguration config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json").Build();
        public String namConfigurationEndpoint;
        public String namClientId;
        public String namClientSecret;
        public String namBaseUrl;
        public String namRedirectUri;

        public TetraService()
        {
            namConfigurationEndpoint = config["namConfigurationEndpoint"];
            namClientId = config["namClientId"];
            namClientSecret = config["namClientSecret"];
            namBaseUrl = config["namBaseUrl"];
            namRedirectUri = config["namRedirectUri"];

            client = new HttpClient();

        }

        public virtual async Task<Response<ConfigurationModel>> GetNAMConfiguration()
        {
            try
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                var json = HttpResponseGet(namConfigurationEndpoint);
                var serialized = JsonSerializer.Deserialize<ConfigurationModel>(json.Result);
                return new Response<ConfigurationModel> { Result = serialized };
            }
            catch (TetraServiceException ex)
            {
                return Response<ConfigurationModel>.CreateErrorResponse(ex);
            }
        }


        public virtual async Task<Response<TokenModel>> GetToken(string authCode)
        {
            //Create object for token body parameters
            TokenRequest paramItem = new TokenRequest(namClientId, namClientSecret, authCode, "authorization_code", namRedirectUri);
            Dictionary<string, string>? dict = Utility.ToDictionary<string>(paramItem);

            try
            {
                var url = namBaseUrl + "/token";
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                var json = HttpResposePost(url, dict);
                var serialized = JsonSerializer.Deserialize<TokenModel>(json.Result);
                return new Response<TokenModel> { Result = serialized };

            }
            catch (TetraServiceException ex)
            {
                return Response<TokenModel>.CreateErrorResponse(ex);
            }
        }


        public virtual async Task<Response<TokenModel>> RefreshToken(string refreshToken)
        {
            //Create object for token body parameters
            RefreshTokenRequest paramItem = new RefreshTokenRequest(namClientId, namClientSecret, "refresh_token", refreshToken);
            Dictionary<string, string>? dict = Utility.ToDictionary<string>(paramItem);

            try
            {
                var url = namBaseUrl + "/token";
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                var json = HttpResposePost(url, dict);
                var serialized = JsonSerializer.Deserialize<TokenModel>(json.Result);
                return new Response<TokenModel> { Result = serialized };

            }
            catch (TetraServiceException ex)
            {
                return Response<TokenModel>.CreateErrorResponse(ex);
            }
        }

        public virtual async Task<Response<RevokeTokenModel>> RevokeToken(string refreshToken)
        {
            //Create object for token body parameters
            RevokeTokenRequest revokeTokenRequest = new RevokeTokenRequest(namClientId, namClientSecret, refreshToken);
            Dictionary<string, string>? dict = Utility.ToDictionary<string>(revokeTokenRequest);

            try
            {
                var url = namBaseUrl + "/revoke";
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                var json = HttpResponsePostNoBody(url, dict);
                var serialized = JsonSerializer.Deserialize<RevokeTokenModel>(json.Result);
                return new Response<RevokeTokenModel> { Result = serialized };

            }
            catch (TetraServiceException ex)
            {
                return Response<RevokeTokenModel>.CreateErrorResponse(ex);
            }
        }
        public virtual async Task<Response<TokenInfoModel>> GetTokenInfo(String token)
        {
            var url = "https://mydevlogin.luxottica.com/nidp/oauth/v1/nam/introspect";
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            var byteArray = Encoding.ASCII.GetBytes($"{this.namClientId}:{this.namClientSecret}");
            string encodeString = Convert.ToBase64String(byteArray);

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", encodeString);
            var infoTokenRequest = new TokenIntrospectRequest(token);
            Dictionary<string, string>? dict = Utility.ToDictionary<string>(infoTokenRequest);

            try
            {
                var json = HttpResposePost(url, dict);
                var serialized = JsonSerializer.Deserialize<TokenInfoModel>(json.Result);
                return new Response<TokenInfoModel> { Result = serialized };

            }
            catch (TetraServiceException ex)
            {
                return Response<TokenInfoModel>.CreateErrorResponse(ex);
            }
        }


        internal async Task<string> HttpResponseGet(string url)
        {
            var req = new HttpRequestMessage(HttpMethod.Get, url);
            var res = await client.SendAsync(req);
            var json = res.Content.ReadAsStringAsync().Result;
            return json;
        }

        internal async Task<string> HttpResposePost(string url, Dictionary<string, string> dict)
        {
            var req = new HttpRequestMessage(HttpMethod.Post, url) { Content = new FormUrlEncodedContent(dict) };
            var res = await client.SendAsync(req);
            var json = await res.Content.ReadAsStringAsync();
            return json;

        }

        internal async Task<string> HttpResponsePostNoBody(string url, Dictionary<string, string> dict)
        {
            var res = await client.PostAsync(url, new FormUrlEncodedContent(dict));
            var json = await res.Content.ReadAsStringAsync();
            if (res.IsSuccessStatusCode)
            {
                if (String.IsNullOrEmpty(json))
                {
                    json = "{\"isSuccess\": true }";
                }
            }

            return json;
        }
    }
}